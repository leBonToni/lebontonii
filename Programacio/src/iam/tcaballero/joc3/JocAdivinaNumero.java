package iam.tcaballero.joc3;

import java.util.Scanner;


public class JocAdivinaNumero extends Joc implements Jugable {
	private int numAdivinar;
	protected int vides;
	Scanner scann = new Scanner(System.in);
	
	public JocAdivinaNumero(int numVides,int numAdivinar) {
		super(numVides);
		this.numAdivinar = numAdivinar;
	}
	
	public void juega() {
		reiniciarPartida();
		Scanner scann = new Scanner(System.in);
		int n = 0;
		
		while(this.numAdivinar!=n) {
			System.out.println("Introdueix el numero que vols esbrinar: ");
			n = scann.nextInt();
			if(validaNumero(n)) {
				if(this.numAdivinar == n) {
				System.out.println("Has acertat");
				actualitzarRecord();
				break;
				}
			
				else {
					if(restaVida()) {
						if(this.numAdivinar > n) {
							System.out.println("El nombre a adivinar és més gran");
						}
						else {
							System.out.println("El nombre a adivinar és més petit");
						}
					}
					else {
						System.out.println("T'has quedat sense vides");
						break;
					}
				}
			}
			else {
				if(restaVida()) {
					if(this.numAdivinar > n) {
						System.out.println("El nombre a adivinar és més gran");
					}
					else {
						System.out.println("El nombre a adivinar és més petit");
					}
				}
				else {
					System.out.println("T'has quedat sense vides");
					break;
				}
			}
		}
	}
	
	
	public boolean validaNumero(int n) {
		return true;
	}

	@Override
	public void muestraNombre() {
		System.out.println("Adivina Numero");		
	}

	@Override
	public void muestraInfo() {
		System.out.println("Has d'adivinar un numero");
		System.out.println("Tens " + mostraVides() + " intents");
		
	}
}

