package iam.tcaballero.joc3;

import java.util.Scanner;

import iam.tcaballero.museu.Escultura;

public class Aplicacio {

	public static void main(String[] args) {		
		Aplicacio ap = new Aplicacio();
		Jugable jug = ap.escullJoc();
		jug.muestraInfo();
		jug.juega();		
	}
	
	public Jugable escullJoc() {
		int n;
		JocAdivinaNumero joc = new JocAdivinaNumero(3, 3);
		JuegoAdivinaPar jocPar = new JuegoAdivinaPar(3, 6);
		JocAdivinaImpar jocImpar = new JocAdivinaImpar(3,7);
		
		Jugable[] jugables = {joc,jocPar,jocImpar};
		
		System.out.println("Escull el Joc");
		System.out.print("0 "); joc.muestraNombre();
		System.out.print("1 "); jocPar.muestraNombre();
		System.out.print("2 "); jocImpar.muestraNombre();
		
		do {
		Scanner scann = new Scanner(System.in);
		n = scann.nextInt();
		} while(n<0 || n>2);
		
		return jugables[n];
	}
	
	
}
