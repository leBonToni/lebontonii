package iam.tcaballero.joc3;

abstract public class Joc {
	private int numVidesInicial;
	private int numVides;
	private static int record = 0;
	
	public Joc (int numVides) {
		this.numVidesInicial = numVides;
		this.numVides = this.numVidesInicial;
	}
	
	public int mostraVides() {
		return numVides;
	}
	
	public boolean restaVida() {
		this.numVides -= 1;
		if(this.numVides == 0) {
			System.out.println("Joc Acabat");
			return false;
		}
		return true;
	}
	
	public void reiniciarPartida() {
		this.numVides = this.numVidesInicial;
	}
	
	public void actualitzarRecord() {
		if(numVides == record) 
			System.out.println("Record Igualat");
		else if(numVides > record) {
			this.record = this.numVides;
			System.out.println("Record Superat: " + this. record);
		}
	}
}
