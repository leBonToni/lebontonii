package iam.tcaballero.joc3;

public class JuegoAdivinaPar extends JocAdivinaNumero {

	public JuegoAdivinaPar(int numVides, int numAdivinar) {
		super(numVides, numAdivinar);
	}
	
	public boolean validaNumero(int n) {
		if(n%2==0) {
			return true;
		}
		System.out.println("Error");
		return false;
	}
	
	@Override
	public void muestraNombre() {
		System.out.println("Adivina un numero Par");	
	}
	
	@Override
	public void muestraInfo() {
		System.out.println("Has d'adivinar un numero Par");
		System.out.println("Tens " + mostraVides() + " intents");
	}
}
