package iam.tcaballero.joc3;

public class JocAdivinaImpar extends JocAdivinaNumero {

	public JocAdivinaImpar(int numVides, int numAdivinar) {
		super(numVides, numAdivinar);
	}
	
	public boolean validaNumero(int n) {
		if(n%2!=0) {
			return true;
		}
		System.out.println("Error");
		return false;
	}
	
	@Override
	public void muestraNombre() {
		System.out.println("Adivina un numero impar");
	}
	
	@Override
	public void muestraInfo() {
		System.out.println("Has d'adivinar un numero Impar");
		System.out.println("Tens " + mostraVides() + " intents");
	}
}
