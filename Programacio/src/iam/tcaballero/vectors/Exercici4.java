package iam.tcaballero.vectors;

import java.util.Collections;
import java.util.Vector;

import javax.swing.JOptionPane;

public class Exercici4 {

	public static void main(String[] args) {
		
		Vector<Integer> v = new Vector<Integer>(20);

		for (int i = 0; i < v.capacity(); i++) {
			v.add(Integer.parseInt(JOptionPane.showInputDialog("Fica-me'l")));
		}
		
		Collections.sort(v);
		
		System.out.println(v);
	}
}
