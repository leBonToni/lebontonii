package iam.tcaballero.vectors;

import java.util.Collections;
import java.util.Random;
import java.util.Vector;

public class Exercici2 {
	
	public static int midaGrup = 35;

	public static void main(String[] args) {
		Random r = new Random();
		Vector<Integer> v = new Vector<Integer>(midaGrup);
		
		for (int i = 0; i < v.capacity() ; i++) {
			int d = r.nextInt(11);
			v.add(d);
		}

		Collections.sort(v);
		
		for (int i = 0; i < v.size(); i++) {
			if(v.get(i) <= 4 && v.get(i) >= 0) {
				System.out.println("Insuficient " + v.get(i));
			}
			else if(v.get(i) <= 6 && v.get(i) >= 5) {
				System.out.println("Aprovat " + v.get(i));
			}
			else if(v.get(i) <= 8 && v.get(i) >= 7) {
				System.out.println("Notable " + v.get(i));
			}
			else if(v.get(i) <= 10 && v.get(i) >= 9) {
				System.out.println("Execel·lent " + v.get(i));
			}
		}
		
		

	}
}
