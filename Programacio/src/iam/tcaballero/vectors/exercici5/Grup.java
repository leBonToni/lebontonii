package iam.tcaballero.vectors.exercici5;

import java.util.Vector;

public class Grup {
	Vector<Estudiant> e = new Vector<Estudiant>();
	Professor tutor;

	public Grup(Vector<Estudiant> estudiants, Professor tutor) {
		this.e = estudiants;
		this.tutor = tutor;
	}

	public Vector<Estudiant> getEstudiants() {
		return e;
	}

	public int numAprovats() {
		int numAprovats = 0;
		for (int i = 0; i < e.size(); i++) {
			if (e.get(i).getNota() >= 5)
				numAprovats++;
		}

		return numAprovats;
	}

	public String toString() {
		String estudiantsString = "Els Estudiants son: ";
		for (int i = 0; i < e.size(); i++) {
			estudiantsString = estudiantsString + e.get(i) + " ";
		}

		return estudiantsString + " i el tutor és" + tutor;
	}
}
