package iam.tcaballero.vectors.exercici5;

import java.util.Scanner;
import java.util.Vector;

public class TestClase {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		 int n;
		do {
			System.out.print("Introdueix la quantitat d'alumnes que vols: ");
			n = scan.nextInt();
		} while (n < 1);
		
		Vector<Estudiant> grpEst = new Vector<Estudiant>(n);
		
		String nom, cognom, DNI, curs;
		int edat, codiAlumne, nota, codiProfessor;
		
		for (int i = 0; i < n; i++) {
			System.out.println("Alumne num " + i);
			System.out.println("nom: ");
			nom = scan.next();
			
			System.out.println("cognom: ");
			cognom = scan.next();
			
			System.out.println("DNI: ");
			DNI = scan.next();
			
			System.out.println("curs: ");
			curs = scan.next();
			
			System.out.println("edat: ");
			edat = scan.nextInt();
			
			System.out.println("codiAlumne: ");
			codiAlumne = scan.nextInt();
			
			System.out.println("nota: ");
			nota = scan.nextInt();
			
			Estudiant e = new Estudiant(nom, cognom, DNI, curs, edat, codiAlumne,nota);
			grpEst.addElement(e);
		}
		
		System.out.println("Introdueix les dades del tutor: ");
		System.out.println("nom: ");
		nom = scan.next();
		
		System.out.println("cognom: ");
		cognom = scan.next();
		
		System.out.println("DNI: ");
		DNI = scan.next();
		
		System.out.println("edat: ");
		edat = scan.nextInt();
		
		System.out.println("codiProfessor: ");
		codiProfessor = scan.nextInt();
		
		Professor tutor = new Professor(nom, cognom, DNI, edat, codiProfessor);
		
		Grup grupA = new Grup(grpEst, tutor);
		
		System.out.println(grupA.numAprovats());
		System.out.println(grupA);
		
		scan.close();
	}
}
