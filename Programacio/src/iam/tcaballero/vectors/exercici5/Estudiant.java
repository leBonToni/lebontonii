package iam.tcaballero.vectors.exercici5;

public class Estudiant {
	private String nom, cognom, DNI, curs;
	private int edat, codiAlumne, nota;
	
	public Estudiant (String nom, String cognom, String DNI, String curs, int edat, int codiAlumne, int nota) {
		this.nom = nom;
		this.cognom = cognom;
		this.DNI = DNI;
		this.curs = curs;
		this.edat = edat;
		this.codiAlumne = codiAlumne;
		this.nota = nota;
	}

	public String toString() {
		return "Estudiant [nom=" + nom + ", cognom=" + cognom + ", DNI=" + DNI
				+ ", curs=" + curs + ", edat=" + edat + ", codiAlumne="
				+ codiAlumne + ", nota=" + nota + "]";
	}

	public int getNota() {
		return nota;
	}
	
}
