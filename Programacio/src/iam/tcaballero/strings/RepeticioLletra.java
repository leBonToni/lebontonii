package iam.tcaballero.strings;

import java.util.Scanner;

public class RepeticioLletra {

	public static void main(String[] args) {
		Scanner n = new Scanner(System.in);
		System.out.println("Frase:");
			
		String frase = n.next();
		frase.trim();
		
		System.out.println("Lletra: ");
		String s = n.next();
		char c = s.charAt(0);		
		
		int t = 0;
		for (int i = 0; i < frase.length(); i++) {
			if(frase.charAt(i) == c) { t++;}
		}
		
		System.out.printf("És repeteix %d vegades",t);
		
	}
	
}
