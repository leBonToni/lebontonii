package iam.tcaballero.strings;

import java.util.Scanner;

public class Palindrom {
	
	public static void main(String[] args) {
		Scanner n = new Scanner(System.in);
		boolean esPal = false;
		String paraula = "";
		
		System.out.print("Paraula: ");
		paraula = n.next();
		String paraulaGirada = "";	
		
		for(int i = paraula.length(); i > 0; i--) {
			paraulaGirada = paraulaGirada + paraula.charAt(i-1);
		}
		
		
		if(paraula.compareTo(paraulaGirada)==0) {
			esPal = true;
		}
		
		if(esPal) {
			System.out.println("Palindroms");
		} else {
			System.out.println("No");
		}
		
		
	}
	
}
