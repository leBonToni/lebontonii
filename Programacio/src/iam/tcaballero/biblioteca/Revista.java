package iam.tcaballero.biblioteca;

public class Revista extends Fitxa {
	private short any;
	private short nro;	
	
	public Revista(String ref,String taitol,short nro) {
		this.referencia = ref;
		this.taitol = taitol;
		this.nro = nro;
	}
	
	public void setAny(short any) {
		this.any = any;
	}
	
	public void setNro(short nro) {
		this.nro = nro;
	}
	
	public short getAny() {
		return any;
	}
	
	public short getNro() {
		return nro;
	}
	
}
 