package iam.tcaballero.biblioteca;

public class Obra extends Fitxa{
	String autor;
	short nrPags;
	static int numObres;
	
	public Obra(String ref,String taitol,String autor) {
		this.referencia = ref; 
		this.taitol = taitol;
		this.autor = autor;
		this.numObres++;
	}
	
	public void setAutor(String autor) {
		this.autor = autor;
	}
	
	public void setNro(short nrPags) {
		this.nrPags = nrPags;
	}
	
	public String getAutor() {
		return autor;
	}
	
	public short getnrPags() {
		return nrPags;
	}
	
	public static int getNumObres() {
		return numObres;
	}
}
