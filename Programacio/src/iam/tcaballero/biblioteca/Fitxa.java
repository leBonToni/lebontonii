package iam.tcaballero.biblioteca;

public class Fitxa {
	String referencia;
	String taitol;
	
	public void setReferencia(String ref) {
		this.referencia = ref;
	}

	public void setTaitol(String taitol) {
		this.taitol = taitol;
	}
	
	public String getReferencia(){
		return referencia;
	}
	
	public String getTaitol(){
		return taitol;
	}
	
	public String toString() {
		return "Referencia: " + referencia;
	}
	
	public boolean equals(Object obj){		
		if(obj instanceof Fitxa)
			if(this.referencia.equals(((Fitxa) obj).referencia))
				return true;
		return false;
	}
}
	

