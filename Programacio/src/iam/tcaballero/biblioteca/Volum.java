package iam.tcaballero.biblioteca;

public class Volum extends Obra{
	private short nro;
	
	public Volum(String ref,String taitol,String autor, short nro) {
		super(ref,taitol,autor);
		this.nro = nro;		
	}
	
	public void setNro(short nro) {
		this.nro = nro;
	}
	
	public short getNro(){
		return nro;
	}
	
}
