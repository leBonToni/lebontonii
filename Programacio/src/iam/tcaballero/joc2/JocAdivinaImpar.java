package iam.tcaballero.joc2;

public class JocAdivinaImpar extends JocAdivinaNumero {

	public JocAdivinaImpar(int numVides, int numAdivinar) {
		super(numVides, numAdivinar);
	}
	
	public boolean validaNumero(int n) {
		if(n%2!=0) {
			return true;
		}
		System.out.println("Error");
		return false;
	}
	
}
