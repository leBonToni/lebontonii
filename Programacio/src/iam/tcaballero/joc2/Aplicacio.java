package iam.tcaballero.joc2;

public class Aplicacio {

	public static void main(String[] args) {
		JocAdivinaNumero joc = new JocAdivinaNumero(3, 3);
		JuegoAdivinaPar jocPar = new JuegoAdivinaPar(3, 6);
		JocAdivinaImpar jocImpar = new JocAdivinaImpar(3,7);
		
		joc.juega();
		jocPar.juega();
		jocImpar.juega();
	}

}
