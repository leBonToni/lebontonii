package iam.tcaballero.abstracte;

public class TestAbstracte {
	public static void main(String[] args) {
		
		//Ex1 Objecte de tipus Shape, que no 
		//té definit el metode getRadius de la classe Circle
		//perque funcioni hem de realitzar casting a Circle
		Shape s1 = new Circle("RED", false,5.5);  
		System.out.println(s1);                   
		System.out.println(s1.getArea());          
		System.out.println(s1.getPerimeter());    
		System.out.println(s1.getColor());
		System.out.println(s1.isFilled());
		System.out.println(((Circle)s1).getRadius());
		
		//Ex2 No falla, s'inicialitza un objecte del tipus Circle igualant-lo a un Objecte
		//del tipus Shape castejat a Circle.
		Circle c1 = (Circle)s1;                  
		System.out.println(c1);
		System.out.println(c1.getArea());
		System.out.println(c1.getPerimeter());
		System.out.println(c1.getColor());
		System.out.println(c1.isFilled());
		System.out.println(c1.getRadius());
		
		//Exemple 3 Objecte del tipus Shape que no té definit el metode
		//getLength de la classe Rectangle,sha de realitzar casting a Rectangle
		Shape s3 = new Rectangle("RED", false, 1.0, 2.0);  
		System.out.println(s3);
		System.out.println(s3.getArea());
		System.out.println(s3.getPerimeter());
		System.out.println(s3.getColor());
		System.out.println(((Rectangle)s3).getLength());
		   
		//Exemple 4 No falla, ja que s'iguala el Objecte rectangle a un objecte
		//Shape al que se li fa casting a Rectangle
		Rectangle r1 = (Rectangle)s3;  
		System.out.println(r1);
		System.out.println(r1.getArea());
		System.out.println(r1.getColor());
		System.out.println(r1.getLength());
		   
		//Exemple 5 Objecte del tipus Shape que no té definit el metode getSide
		//de la classe Square, per tant hem de realitzar un casting a Square
		Shape s4 = new Square(6.6);     
		System.out.println(s4);
		System.out.println(s4.getArea());
		System.out.println(s4.getColor());
		System.out.println(((Square)s4).getSide());
		  
		//Exemple 6 Objecte del tipus Shape que se li fa casting a un objecte del tipus Rectangle, que no
		//té definit el metode getSide, per tant hem de fer casting a Square per poder
		//utiltizar aquest metode, però els dos costats del rectangle seràn iguals
		Rectangle r2 = (Rectangle)s4;
		System.out.println(r2);
		System.out.println(r2.getArea());
		System.out.println(r2.getColor());
		System.out.println(((Square)r2).getSide());
		System.out.println(r2.getLength());
		   
		// Exemple 7 No falla perque Square extends de Rectangle i per tant al fer-li casting té els mateixos
		//atributs.
		Square sq1 = (Square)r2;
		System.out.println(sq1);
		System.out.println(sq1.getArea());
		System.out.println(sq1.getColor());
		System.out.println(sq1.getSide());
		System.out.println(sq1.getLength());		
	}
}
