package iam.tcaballero.abstracte;

public class Square extends Rectangle {
	
	public Square() {
	}
	
	public Square(double side) {
		super(side,side);
	}
	
	public Square(String color, boolean filled, double side) {
		super(color,filled,side,side);
	}
	
	public double getSide() {
		return width;
	}
	
	public void setSide(double side) {
		this.width = side;
		this.lenght = side;
	}
	
	public double getArea() {
		return width * lenght;
	};
	 public double getPerimeter() {
		 return 2 * width + lenght * 2;
	 }
	 public String toString() {
		 return "Color: " + color + " Filled: " + filled + " Width: " + width + "Lenght: " + lenght; 
	 };
}

