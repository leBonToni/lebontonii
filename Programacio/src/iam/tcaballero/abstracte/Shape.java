package iam.tcaballero.abstracte;

abstract public class Shape {
	String color;
	boolean filled;
	
	public Shape() {	
	}
	
	public Shape(String color, boolean filled) {
		this.color = color;
		this.filled = filled;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	public boolean isFilled() {
		if(this.filled) return true;
		return false;
	}

	public void setFilled(boolean filled) {
		if(filled) this.filled = true;
		else this.filled = false;
	}
	
	abstract public double getArea();
	abstract public double getPerimeter();
	abstract public String toString();
}	