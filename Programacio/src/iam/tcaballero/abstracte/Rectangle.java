package iam.tcaballero.abstracte;

public class Rectangle extends Shape {
	double width;
	double lenght;
	
	public Rectangle() {
	}

	public Rectangle(double width, double lenght) {
		this.width = width;
		this.lenght = lenght;
	}
	
	public Rectangle(String color, boolean filled, double width, double lenght) {
		super(color,filled);
		this.width = width;
		this.lenght = lenght;
	}

	public double getWidth() {
		return width;
	}
	
	public void setRadius(double width) {
		this.width = width;
	}
	
	public double getLength() {
		return lenght;
	}
	
	public void setLength(double lenght) {
		this.lenght = lenght;
	}
	
	public double getArea() {
		return width * lenght;
	};
	 public double getPerimeter() {
		 return 2 * width + lenght * 2;
	 }
	 public String toString() {
		 return "Color: " + color + " Filled: " + filled + " Width: " + width + "Lenght: " + lenght; 
	 };
}
