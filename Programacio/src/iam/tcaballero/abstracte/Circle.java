package iam.tcaballero.abstracte;

public class Circle extends Shape{
	double radius;
	
	public Circle() {
	}

	public Circle(double radius) {
		this.radius = radius;
	}
	
	public Circle(String color, boolean filled, double radius) {
		super(color,filled);
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getArea() {
		return Math.PI * radius * radius;
	};
	 public double getPerimeter() {
		 return 2 * Math.PI * radius;
	 }
	 public String toString() {
		 return "Color: " + color + " Filled: " + filled + " Radius: " + radius; 
	 };
	 
}
