package iam.tcaballero.lligafile;

import java.io.Serializable;

public class Jugador implements Serializable{
	
	private int dorsalJugador;
	private String cognomJugador;
	private String nomJugador;
	private int edatJugador;
	private int golsJugador;
	private int acumulatsJugador;
	
	public Jugador(String stringJugador) throws FaltenAtributsJugadorException {
		
		String[] parametresJugador = stringJugador.split("#");
		if(parametresJugador.length < 6) throw new FaltenAtributsJugadorException("Falten Atributs al jugador");
		
		this.dorsalJugador = Integer.parseInt(parametresJugador[0]);
		this.cognomJugador = parametresJugador[1];
		this.nomJugador = parametresJugador[2];
		this.edatJugador = Integer.parseInt(parametresJugador[3]);
		this.golsJugador = Integer.parseInt(parametresJugador[4]);
		this.acumulatsJugador = Integer.parseInt(parametresJugador[5]);
		
	}

	public int getDorsalJugador() {
		return dorsalJugador;
	}

	public void setDorsalJugador(int dorsalJugador) {
		this.dorsalJugador = dorsalJugador;
	}

	public String getCognomJugador() {
		return cognomJugador;
	}

	public void setCognomJugador(String cognomJugador) {
		this.cognomJugador = cognomJugador;
	}

	public String getNomJugador() {
		return nomJugador;
	}

	public void setNomJugador(String nomJugador) {
		this.nomJugador = nomJugador;
	}

	public int getEdatJugador() {
		return edatJugador;
	}

	public void setEdatJugador(int edatJugador) {
		this.edatJugador = edatJugador;
	}

	public int getGolsJugador() {
		return golsJugador;
	}

	public void setGolsJugador(int golsJugador) {
		this.golsJugador = golsJugador;
	}

	public int getAcumulatsJugador() {
		return acumulatsJugador;
	}

	public void setAcumulatsJugador(int acumulatsJugador) {
		this.acumulatsJugador = acumulatsJugador;
	}

	@Override
	public String toString() {
		return "Jugador [dorsalJugador=" + dorsalJugador + ", nomJugador=" + nomJugador + ", acumulatsJugador="
				+ acumulatsJugador + "]";
	}

	
	
	
	
}
