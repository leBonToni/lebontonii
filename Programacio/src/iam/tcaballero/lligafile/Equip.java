package iam.tcaballero.lligafile;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;

public class Equip implements Serializable{

	private String nomEquip;
	private int puntsLliga;
	private Hashtable<Integer,Jugador> jugadorsEquip = new Hashtable<Integer,Jugador>();
	
	public Equip(String nom) {
		this.nomEquip = nom;
		this.puntsLliga = 0;
	}
	
	public Equip(File f) throws IOException,FaltenJugadorsException{
		
		if(f.length() < 15) {
			throw new FaltenJugadorsException("No s'ha creat el equip " + f.getName() + ", perque falten jugadors");
		}
		
		puntsLliga = 0;
		nomEquip = f.getName();
		
		BufferedReader b = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
		String dadesJugador;
		
		while ((dadesJugador = b.readLine()) != null) {
			try {
				Jugador j = new Jugador(dadesJugador);
				jugadorsEquip.put(j.getDorsalJugador(),j);
			} catch (FaltenAtributsJugadorException e) {
				System.out.println("No s'ha creat el jugador, falten atributs");
			}
		}
			
	}
	
	public byte[] afegirJugadors(ArrayList<String> jugadorsEquips) throws IOException, FaltenAtributsJugadorException {	
		
		ByteArrayOutputStream bOut = new ByteArrayOutputStream();
		DataOutputStream dOut = new DataOutputStream(bOut);
		
		for (String jugador : jugadorsEquips) {
			try {
				Jugador j = new Jugador(jugador);
				jugadorsEquip.put(j.getDorsalJugador(),j);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			String s= jugador + "\n";
			dOut.write(s.getBytes());
		}
		
		byte[] b = bOut.toByteArray();
		
		return b;
		
	}
	
public byte[] afegirJugador(String jugadorEquip) throws IOException, FaltenAtributsJugadorException {	
		
		ByteArrayOutputStream bOut = new ByteArrayOutputStream();
		DataOutputStream dOut = new DataOutputStream(bOut);
		
		try {
			Jugador j = new Jugador(jugadorEquip);
			jugadorsEquip.put(j.getDorsalJugador(),j);
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		String s= jugadorEquip + "\n";
		dOut.write(s.getBytes());
			
		byte[] b = bOut.toByteArray();
		
		return b;
		
	}
		
	public Hashtable<Integer, Jugador> getJugadorsEquip() {
		return jugadorsEquip;
	}

	public void setJugadorsEquip(Hashtable<Integer, Jugador> jugadorsEquip) {
		this.jugadorsEquip = jugadorsEquip;
	}

	public String getNom(){
		return nomEquip;
	}
	
	public int getPuntsLliga(){
		return puntsLliga;
	}
	
	public void incrementaPunts(int punts){
		this.puntsLliga += punts;
	}
	
	public String toString(){
		return "Equip: " + getNom() + ", Punts: " + getPuntsLliga(); 
	}
	
}
