package iam.tcaballero.lligafile;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import javax.swing.JOptionPane;


public class Menu {
	LLiga l;
	Path p;
	private boolean sufEquips = false, lligaSeleccionada = false;
	
	public Menu() {
		super();
	}
	
	public void menu(int o) {
		
		switch (o) {
		case 1:
			String nomLligaCre = JOptionPane.showInputDialog("Nom Lliga");
			crearLliga(nomLligaCre);
			break;
			
		case 2:
			String nomLligaSel = JOptionPane.showInputDialog("Nom Lliga Selecció");
			seleccionarLliga(nomLligaSel);
			break;
	
		case 3:
			jugarLliga();
			break;

		case 4:
			afegirEquipIJugadors();
			break;
			
		case 5:
			modificarEquip();
			break;
			
		case 6:
			exportarLligaBinari();
			break;
		
		case 7:
			importarLligaBinari();
			break;
			
		default:
			break;
		}
	}
	
	private boolean importarLligaBinari() {
		if(lligaSeleccionada) {
			try {
				ObjectInputStream obIn = new ObjectInputStream(new FileInputStream(p.toString()+"Binari"));
				l = (LLiga) obIn.readObject();
				return true;
			} catch(IOException e) {
				e.printStackTrace();
				return false;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				return false;
			}
		}
		
		else {
			System.out.println("Has de seleccionar la lliga primer");
			return false;
		}
		
	}
	
	private boolean exportarLligaBinari() {
		if(lligaSeleccionada) {
			try {
				ObjectOutputStream obOut = new ObjectOutputStream(new FileOutputStream(p.toString()+"Binari"));
				obOut.writeObject(l);
				obOut.close();
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		
			return true;
			}
			else {
				System.out.println("Has de seleccionar la lliga primer");
				return false;
			}
	}
	
	private void afegirEquipIJugadors() {
		if (lligaSeleccionada) {
			String nomEquip = JOptionPane.showInputDialog("Nom Equip");
			
			ArrayList<String> jugadorsEquip = new ArrayList<>();
			for (int i = 0; i < 15; i++) {
				jugadorsEquip.add(JOptionPane.showInputDialog("Jugador"));
			}
			
			try {
				this.l.afegirEquip(nomEquip, jugadorsEquip);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} else {
			System.out.println("Has de seleccionar la lliga primer");
		}
		
	}
	
	private void modificarEquip() {
		if (lligaSeleccionada) {
			String nomEquip = JOptionPane.showInputDialog("Nom Equip");
			
			String jugadorEquip = (JOptionPane.showInputDialog("Jugador"));
			
			try {
				this.l.modificarEquip(nomEquip, jugadorEquip);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} else {
			System.out.println("Has de seleccionar la lliga primer");
		}
	}
	
	private void jugarLliga() {
		if(sufEquips) {
			this.l.jugaLliga();
		}
		else {
			System.out.println("No hi ha suficients equips a la lliga com per jugar-la, com a minim"
					+ "es necessiten 2");
		}
	}

	private boolean seleccionarLliga(String nom) {
		this.p = Paths.get(nom);
			
		try {
			this.l = new LLiga(p);
			this.lligaSeleccionada = true;
			if(this.l.getQuantitatEquips() > 1) {
				this.sufEquips = true;
			}
			return true;
		} catch (IOException e) {
			return false;
		}
		
	}

	private boolean crearLliga(String nom) {
		try {
			this.sufEquips = false;
			this.p = Paths.get(nom);
			Files.createDirectory(this.p);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	
	
	
}
