package iam.tcaballero.lligafile;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Test {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Menu m = new Menu();
		
		int o;
		do {
			 o = Integer.parseInt(JOptionPane.showInputDialog("Opció\n"
			 		+ "1) Crear Lliga\n"
			 		+ "2) Seleccionar Lliga\n"
			 		+ "3) Jugar Lliga\n"
			 		+ "4) Afegir Equips i Jugadors\n"
			 		+ "5) Modificar Equip\n"
			 		+ "6) Exportar LLiga Binari\n"
			 		+ "7) Importar Lliga Binari"));
			m.menu(o);
		} while(o != 6);
	}

}
