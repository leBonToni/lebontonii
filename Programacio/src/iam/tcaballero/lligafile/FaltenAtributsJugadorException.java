package iam.tcaballero.lligafile;

public class FaltenAtributsJugadorException extends Exception {

	public FaltenAtributsJugadorException(String message) {
		super(message);
	}
	
}
