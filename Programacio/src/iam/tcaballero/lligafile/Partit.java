package iam.tcaballero.lligafile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;

public class Partit implements Serializable{

	private Equip equipLocal;
	private Equip equipVisitant;
	private ArrayList<Gol> golsEquipLocal;
	private ArrayList<Gol> golsEquipVisitant;
		
	public Partit(Equip e1,Equip e2){	
		equipLocal = e1;
		equipVisitant = e2;
		this.golsEquipLocal = new ArrayList<>();
		this.golsEquipVisitant = new ArrayList<>();
	}
		
	public void jugaPartit() {
		Random r = new Random();
		int x = r.nextInt(6), y = r.nextInt(6), z, m;
		
		Hashtable<Integer,Jugador> jugadorsLocal = equipLocal.getJugadorsEquip();
		Hashtable<Integer,Jugador> jugadorsVisitant = equipVisitant.getJugadorsEquip();
		
		for (int i = 0; i < x; i++) {
			z = r.nextInt(15);
			m = r.nextInt(90);
			Gol g = new Gol(jugadorsLocal.get(z),m);
			marcaLocal(g);
		}
		
		for (int i = 0; i < y; i++) {
			z = r.nextInt(15);
			m = r.nextInt(90);
			Gol g = new Gol(jugadorsVisitant.get(z),m);
			marcaVisitant(g);
		}
		
		this.fi();
	}
	
	public void marcaLocal(Gol g) {
		this.golsEquipLocal.add(g);
	}
	
	public void marcaVisitant(Gol g) {
		this.golsEquipVisitant.add(g);
	}
	
	public void setGolsEquipLocal(ArrayList<Gol> g) {
		this.golsEquipLocal = g;
	}
	
	public void setGolsEquipVisitant(ArrayList<Gol> g) {
		this.golsEquipVisitant = g;
	}
	
	public String marcador(){
		return equipLocal.getNom() +" "+ golsEquipLocal.size() +"-"+ golsEquipVisitant.size()+" "+equipVisitant.getNom();
	}
			
	public String fi(){
		if(golsEquipLocal.size() > golsEquipVisitant.size()){
			equipLocal.incrementaPunts(3);
			return this.marcador();
		}
		else if(golsEquipLocal.size() < golsEquipVisitant.size()){ 
			equipVisitant.incrementaPunts(3);
			return this.marcador();
		}
		else{
			equipLocal.incrementaPunts(1);
			equipVisitant.incrementaPunts(1);
			return "Empat";
		}
	}


}
