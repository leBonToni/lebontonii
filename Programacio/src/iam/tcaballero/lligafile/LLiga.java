package iam.tcaballero.lligafile;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class LLiga implements Serializable{ 

	private ArrayList<Equip> equipsLliga = new ArrayList<Equip>();
	private ArrayList<Partit> partitsLliga = new ArrayList<Partit>();
	private int quantitatEquips, numPartits;
	private File f;
	
	public LLiga(Path p) throws IOException{
		
		this.f = new File(p.getFileName().toString());
		File[] fitxersLliga = f.listFiles();
		
		try {
			for (File fitxerequip : fitxersLliga) {
				Equip e = new Equip(fitxerequip);
				equipsLliga.add(e);
			}
		} catch (FaltenJugadorsException e) {
			e.printStackTrace();
		}
		
		this.quantitatEquips = fitxersLliga.length;
		this.numPartits = quantitatEquips *  quantitatEquips - quantitatEquips;
	}
		
	public boolean afegirEquip(String nom, ArrayList<String> jugadorsEquip) throws IOException {		
		Equip e = new Equip(nom);
		Path p = Paths.get(f.getPath() + "/" + e.getNom()); 
		byte[] b = null;
		
		try {
			b = e.afegirJugadors(jugadorsEquip);
		} catch (FaltenAtributsJugadorException e1) {
			e1.printStackTrace();
			return false;
		}
	
		Files.write(p,b);
		crearPartits();
		return true;
	}
	
	public boolean modificarEquip(String nom,String jugadorEquip) throws IOException {		
		Equip e = new Equip(nom);
		Path p = Paths.get(f.getPath() + "/" + e.getNom()); 
		byte[] b = null;
		
		try {
			b = e.afegirJugador(jugadorEquip);
		} catch (FaltenAtributsJugadorException e1) {
			e1.printStackTrace();
			return false;
		}
	
		Files.write(p,b);
		return true;
	}
	
	public ArrayList<Equip> getEquipsLliga() {
		return equipsLliga;
	}

	public void addEquip(Equip e) {
		this.equipsLliga.add(e);
	}

	public void setEquipsLliga(ArrayList<Equip> equipsLliga) {
		this.equipsLliga = equipsLliga;
	}



	public ArrayList<Partit> getPartitsLliga() {
		return partitsLliga;
	}



	public void setPartitsLliga(ArrayList<Partit> partitsLliga) {
		this.partitsLliga = partitsLliga;
	}



	public int getQuantitatEquips() {
		return quantitatEquips;
	}



	public void setQuantitatEquips(int quantitatEquips) {
		this.quantitatEquips = quantitatEquips;
	}



	public int getNumPartits() {
		return numPartits;
	}



	public void setNumPartits(int numPartits) {
		this.numPartits = numPartits;
	}

	private void crearPartits() {
		equipsLliga.removeAll(equipsLliga);
		
		for(int i = 0; i< equipsLliga.size(); i++) {
			for(int j = 0; j < equipsLliga.size(); j++) {
				if(equipsLliga.get(i).equals(equipsLliga.get(j)));
				else {
					Equip e1 = equipsLliga.get(i);
					Equip e2 = equipsLliga.get(j);	
					partitsLliga.add(new Partit(e1,e2));
					
				}
			}
		}
	}

	public void jugaLliga() {
		this.crearPartits();
		for (Partit p : partitsLliga) {
			p.jugaPartit();
		}
	}

	public void classificacio() {
		
		
		for (Equip e : equipsLliga) {
			e.toString();
		}
	}
	
}
