package iam.tcaballero.lliga;

public class Equip {
	
	String nomEquip;
	private int puntsLliga;
	static int numequips = 0;
	
	public Equip(String nom) {
		this.nomEquip = nom;
		this.puntsLliga = 0;
		numequips++;
	}
	
	public String getNom() {
		return nomEquip;
	}
	
	public int getPunts() {
		return puntsLliga;
	}
	
	public void incrementaPunts(int punts) {
		this.puntsLliga += punts;
	}
	
	public int mostraPunts() {
		return puntsLliga;
	}
	
	public String toString() {
		return getNom() + " - " + getPunts() + " Punts";
		
	}
	
	public static int numEquips() {
		return numequips;
	}
}
