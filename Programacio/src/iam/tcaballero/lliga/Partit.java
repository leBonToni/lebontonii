package iam.tcaballero.lliga;

public class Partit {
	
	private Equip equip1;
	private Equip equip2;
	private int golsEquip1;
	private int golsEquip2;
	
	public Partit(Equip e1,Equip e2){	
		this.equip1 = e1;
		this.equip2 = e2;
		this.golsEquip1 = 0;
		this.golsEquip2 = 0; 
	}
	
	public Partit(String nom1, String nom2, int gols1, int gols2) {
		equip1 = new Equip(nom1);
		equip2 = new Equip(nom2);
		this.golsEquip1 = gols1;
		this.golsEquip2 = gols2; 
	}
	
	public void marca1() {
		this.golsEquip1++;
	}
	
	public void marca2() {
		this.golsEquip2++;
	}
	
	public String marcador(){
		return equip1.getNom() + " " + golsEquip1 + "-" + golsEquip2 + " " + equip2.getNom();
	}
	
	public void fi() {
		if(golsEquip1 < golsEquip2) { 
			equip2.incrementaPunts(3);
		}
		else if(golsEquip1 > golsEquip2) {
			equip1.incrementaPunts(3);
		}
		else {
			equip1.incrementaPunts(1);
			equip2.incrementaPunts(1);
		}
	}
	
	
	public String getNomEquip1() {
		return equip1.getNom();
	}
	
	public String getNomEquip2() {
		return equip2.getNom();
	}
	
	public int getGolsEquip1() {
		return golsEquip1;
	}
	
	public int getGolsEquip2() {
		return golsEquip2;
	}
	
	public void setGolsEquip1(int g1) {
		this.golsEquip1 = g1;
	}
	
	public void setGolsEquip2(int g2) {
		this.golsEquip1 = g2;
	}	
}
