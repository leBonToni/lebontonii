package iam.tcaballero.lliga;
import java.util.Scanner;

public class ExecutarLliga {

	public static void main(String[] args) {
		Scanner dades = new Scanner(System.in);
		System.out.println("Introdueix el tamany de la lliga: ");
		int n = dades.nextInt();
		
		Equip equips[] = new Equip[n];
		Partit partits[] = new Partit[n*n-n];
		
		for(int i = 0; i< equips.length; i++) {
			System.out.println("Introdueix-me el nom del equip" + i +": ");
			String nom = dades.next();
			equips[i] = new Equip(nom);
		}
		
		Lliga lliga1 = new Lliga(equips, partits);
		lliga1.jugaLliga();	
		lliga1.classificacio();
		System.out.print(Equip.numEquips());
	}
		
		
		
		
		
		
		
		
}
