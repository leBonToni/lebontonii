package iam.tcaballero.lliga;
import java.util.Random;

public class Lliga {
	Equip equips[];
	Partit partits[];
	
	public Lliga(Equip[] equips, Partit[] partits) {
		this.equips = equips;
		this.partits = partits;
		
		int k = 0;
		for(int i = 0; i< equips.length; i++) {
			for(int j = 0; j < equips.length; j++) {
				if(equips[i].equals(equips[j]));
				else {
					Equip e1 = equips[i];
					Equip e2 = equips[j];	
					partits[k] = new Partit(e1,e2);
					k++;
				}
			}
		}	
	}
	
	public void jugaPartit(int i) {
		Random n = new Random();
		int x = n.nextInt(6), y = n.nextInt(6);
		this.partits[i].setGolsEquip1(x);
		this.partits[i].setGolsEquip2(y);
		this.partits[i].fi();
	}
	
	public void jugaLliga() {
		for(int i = 0; i < partits.length; i++) {
			jugaPartit(i);
		}
	}
	
	public void classificacio() {
		for(int i = 0; i < equips.length; i++) {
			System.out.println(equips[i].toString());
		}
	}
}
