package iam.tcaballero.botiga;

import java.util.ArrayList;
import java.util.Collections;

public class Magatzem {
	ArrayList<Article> llistaArticles;
	ArrayList<Article> articlesForaDStock;
	
	public Magatzem(ArrayList<Article> article) {
		this.llistaArticles = article;
		this.articlesForaDStock = new ArrayList<>();
	}

	public Article buscarArticle(int codi) {
		Article articlesMagatzem = null;
		for (int i = 0; i < llistaArticles.size(); i++) {
			if (codi == llistaArticles.get(i).getCodiArticle()) {
				articlesMagatzem = llistaArticles.get(i);
			}
		}
		return articlesMagatzem;

	}

	public ArrayList<Article> senseStock() {
		for (int i = 0; i < llistaArticles.size(); i++) {
			if (llistaArticles.get(i).getStockDisponible() == 0 || !llistaArticles.get(i).isSuficientStock()) {
				articlesForaDStock.add(llistaArticles.get(i));
			}
		}
		return articlesForaDStock;

	}

	public String articlesMagatzem() {
		String articlesMagatzem = "";
		
		for (int i = 0; i < llistaArticles.size(); i++) {
			articlesMagatzem += llistaArticles.get(i).toString() + "\n";
		}
		return articlesMagatzem;
	}
	
	public String articlesMagatzemOrdenats(){
		String articlesMagatzem = "";
		Collections.sort(llistaArticles);
	
		for (int i = 0; i < llistaArticles.size(); i++) {
			articlesMagatzem += llistaArticles.get(i).toString() + "\n";
		}
		return articlesMagatzem;
	}
}
