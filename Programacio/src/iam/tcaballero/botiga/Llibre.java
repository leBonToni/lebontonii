package iam.tcaballero.botiga;

public class Llibre extends Article{
	private String titolLlibre, autorLlibre;
	private int numeroPag;
	public Llibre(String titolLlibre, String autorLlibre, int numeroPag,int codiArticle, int preuArticle, int stockDisponible, String descArticle) {
		super(codiArticle, preuArticle, stockDisponible, descArticle);
		this.autorLlibre = autorLlibre;
		this.titolLlibre = titolLlibre;
		this.numeroPag = numeroPag;
	}

	public String getTitolLlibre() {
		return titolLlibre;
	}

	public void setTitolLlibre(String titolLlibre) {
		this.titolLlibre = titolLlibre;
	}

	public String getAutorLlibre() {
		return autorLlibre;
	}

	public void setAutorLlibre(String autorLlibre) {
		this.autorLlibre = autorLlibre;
	}

	public int getNumeroPag() {
		return numeroPag;
	}

	public void setNumeroPag(int numeroPag) {
		this.numeroPag = numeroPag;
	}

	@Override
	public String toString() {
		return "Llibre [titolLlibre=" + titolLlibre + ", autorLlibre=" + autorLlibre + ", numeroPag=" + numeroPag + "]";
	}
	
	
}
