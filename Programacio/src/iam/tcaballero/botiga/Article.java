package iam.tcaballero.botiga;

public class Article implements Comparable<Article>{
	private int codiArticle, preuArticle, stockDisponible;
	private String descArticle;
	private boolean suficientStock;
	
	
	public Article(int codiArticle, int preuArticle, int stockDisponible, String descArticle) {
		this.codiArticle = codiArticle;
		this.preuArticle = preuArticle;
		this.stockDisponible = stockDisponible;
		this.descArticle = descArticle;
	}
	
	public int ventaArticle(int unitatsArticle) {
		
		if(this.stockDisponible > 0 && !this.suficientStock) {
			int preuTotal = this.preuArticle * unitatsArticle;
			this.stockDisponible -= unitatsArticle;
			return preuTotal;
		}
		return -1;
	}

	public boolean isSuficientStock() {
		return suficientStock;
	}

	public void setNoSuficientStock(boolean noSuficientStock) {
		this.suficientStock = noSuficientStock;
	}

	public void reposarArticle(int unitatsArticle) {
		this.stockDisponible += unitatsArticle;
	}
		
	public int compareTo(Article a) {
		if(this.codiArticle > a.getCodiArticle()) {
			return 1;
		}
		else if(this.codiArticle < a.getCodiArticle()) {
			return -1;
		}
		else {
			return 0;
		}
	}

	public int getCodiArticle() {
		return codiArticle;
	}

	public void setCodiArticle(int codiArticle) {
		this.codiArticle = codiArticle;
	}

	public int getPreuArticle() {
		return preuArticle;
	}

	public void setPreuArticle(int preuArticle) {
		this.preuArticle = preuArticle;
	}

	public int getStockDisponible() {
		return stockDisponible;
	}

	public void setStockDisponible(int stockDisponible) {
		this.stockDisponible = stockDisponible;
	}

	public String getDescArticle() {
		return descArticle;
	}

	public void setDescArticle(String descArticle) {
		this.descArticle = descArticle;
	}

	public String toString() {
		return "Article [codiArticle=" + codiArticle + ", preuArticle=" + preuArticle + ", stockDisponible="
				+ stockDisponible + ", descArticle=" + descArticle + "]";
	}
	
}
