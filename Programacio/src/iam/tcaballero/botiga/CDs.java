package iam.tcaballero.botiga;

import java.util.ArrayList;

public class CDs extends Article {
	private String titolCD,nomBanda;
	private ArrayList<Canco> cancons;
	private int numCancons;
	
	public CDs(String titolCD, String nomBanda,int codiArticle, int preuArticle, int stockDisponible, String descArticle,ArrayList<Canco> cancons) {
		super(codiArticle, preuArticle, stockDisponible, descArticle);
		this.nomBanda = nomBanda;
		this.numCancons = cancons.size();
		this.titolCD = titolCD;
		this.cancons = cancons;
	}
	
	public String getTitolCD() {
		return titolCD;
	}


	public void setTitolCD(String titolCD) {
		this.titolCD = titolCD;
	}


	public String getNomBanda() {
		return nomBanda;
	}


	public void setNomBanda(String nomBanda) {
		this.nomBanda = nomBanda;
	}


	public int getNumCancons() {
		return numCancons;
	}


	public void setNumCancons(int numCancons) {
		this.numCancons = numCancons;
	}

	@Override
	public String toString() {
		return "CDs [titolCD=" + titolCD + ", nomBanda=" + nomBanda + ", cancons=" + cancons + ", numCancons="
				+ numCancons + "]";
	}

	
	
	
}
