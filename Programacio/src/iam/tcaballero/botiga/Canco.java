package iam.tcaballero.botiga;

public class Canco {
	private String titolCanco;
	private int durada;
	public static int numCanconsTotals;
	
	public Canco(String titolCanco, int durada) {
		this.durada = durada;
		this.titolCanco = titolCanco;
		this.numCanconsTotals++;
	}

	public static int getNumCanconsTotals() {
		return numCanconsTotals;
	}

	public static void setNumCanconsTotals(int numCanconsTotals) {
		Canco.numCanconsTotals = numCanconsTotals;
	}

	public String getTitolCanco() {
		return titolCanco;
	}

	public void setTitolCanco(String titolCanco) {
		this.titolCanco = titolCanco;
	}

	public int getDurada() {
		return durada;
	}

	public void setDurada(int durada) {
		this.durada = durada;
	}

	public String toString() {
		return "Canco [titolCanco=" + titolCanco + ", durada=" + durada + "]";
	}
	
	
}
