package iam.tcaballero.botiga;

public class DVDs extends Article{
	private String titolDVD,directorDVD,idiomaDVD;
	private int duracioDVD;
	
	public DVDs(String titolDVD, String directorDVD, String idiomaDVD, int duracioDVD, int codiArticle, int preuArticle, int stockDisponible, String descArticle) {
		super(codiArticle, preuArticle, stockDisponible, descArticle);
		this.titolDVD = titolDVD;
		this.directorDVD = directorDVD;
		this.idiomaDVD = idiomaDVD;
		this.duracioDVD = duracioDVD;
	}

	public String getTitolDVD() {
		return titolDVD;
	}

	public void setTitolDVD(String titolDVD) {
		this.titolDVD = titolDVD;
	}

	public String getDirectorDVD() {
		return directorDVD;
	}

	public void setDirectorDVD(String directorDVD) {
		this.directorDVD = directorDVD;
	}

	public String getIdiomaDVD() {
		return idiomaDVD;
	}

	public void setIdiomaDVD(String idiomaDVD) {
		this.idiomaDVD = idiomaDVD;
	}

	public int getDuracioDVD() {
		return duracioDVD;
	}

	public void setDuracioDVD(int duracioDVD) {
		this.duracioDVD = duracioDVD;
	}

	@Override
	public String toString() {
		return "DVDs [titolDVD=" + titolDVD + ", directorDVD=" + directorDVD + ", idiomaDVD=" + idiomaDVD
				+ ", duracioDVD=" + duracioDVD + "]";
	}
	
	
}