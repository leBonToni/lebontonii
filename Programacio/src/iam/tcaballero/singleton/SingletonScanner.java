package iam.tcaballero.singleton;

import java.util.Scanner;

public class SingletonScanner {

	private static Scanner instance = null;
	
	protected SingletonScanner() {	
	}

	public static Scanner getInstance() {
	      if(instance == null) {
	         instance = new Scanner(System.in);
	      }
	      return instance;
	   } 
}
