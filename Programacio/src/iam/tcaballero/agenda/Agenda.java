package iam.tcaballero.agenda;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;

public class Agenda {
	
	Hashtable<Integer, Persona> persones;
	
	
	public Agenda() {
		this.persones = new Hashtable<Integer,Persona>();
	}
	
	public Agenda(ArrayList<Persona> p ) {
		this();		
		for (int i = 0; i < p.size(); i++) {
			this.persones.put(p.get(i).getDni(),p.get(i));
		}
	}
	
	public boolean afegirPersona(Persona p){
		if(persones.put(p.getDni(), p) != null) {
			return false;
		}
		return true;
	}
		
	public boolean eliminaPersona(int dni) {
		if(persones.remove(dni) != null) {
			return false;
		}
		return true; 
	}
	
	public Persona buscarPersonaDNI(int dni){
		return persones.get(dni) ;		
	}
	
	public Persona buscarNomPersona(String nom) {
		
		for (Map.Entry<Integer, Persona> persona : persones.entrySet()) {
			if(nom.equals(persona.getValue().getNom())){
				return persona.getValue();
			}	
		}
		return null;
	}

	public Hashtable<Integer, Persona> getPersones() {
		return persones;
	}
}
	
	

