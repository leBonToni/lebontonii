package iam.tcaballero.agenda;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.Scanner;

public class Principal {

	static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		int o = 0;
		Persona p = new Persona();
		//ArrayList<Persona> pe = new ArrayList<Persona>();
		Agenda a = new Agenda();
		
		do {
		
			System.out.println("(1) Afegir Persona");
			System.out.println("(2) Esborrar Persona");
			System.out.println("(3) Buscar Persona per DNI");
			System.out.println("(4) Buscar Persona per NOM");
			System.out.println("(5) Mostrar Agenda");
			System.out.println("(6) Surt");
			System.out.println("Opcio: ");
			o = scanner.nextInt();
			
			switch (o) {
			case 1:
				
				System.out.println("Nom: ");
				String nom = scanner.next();
				
				System.out.println("DNI: ");
				int dni = scanner.nextInt();
				
				System.out.println("Telefon: ");
				int telf = scanner.nextInt();
					
				p.setDni(dni);
				p.setNom(nom);
				p.setTelf(telf);
				
				//Agenda
				a.afegirPersona(p);
				
				break;
				
			case 2:
				
				System.out.println("DNI: ");
				dni = scanner.nextInt();	
	
				if(a.eliminaPersona(dni)){
					System.out.println("Persona eliminada: " + a.buscarPersonaDNI(dni).getNom());
				} else 	
					System.out.println("Persona no trobada");
				
				break;	
			
			case 3:
				
				System.out.println("DNI: ");
				dni = scanner.nextInt();	
				p = a.buscarPersonaDNI(dni);
				if(p != null){
					System.out.println(p.toString());
				} else 	
					System.out.println("Persona no trobada");
				
				
				break;

			case 4:
				
				System.out.println("Nom: ");
				nom = scanner.nextLine();	
				p = a.buscarNomPersona(nom);
				if(p != null){
					System.out.println(p.toString());
				} else 	
					System.out.println("Persona no trobada");
				
				break;
			
			case 5:
				System.out.println(a.getPersones());
				break;
								
			default:
				break;
			}
				
		} while(o != 6) ;
	}
}
