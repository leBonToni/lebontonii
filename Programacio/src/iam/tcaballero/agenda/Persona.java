package iam.tcaballero.agenda;

public class Persona {
	
	private String nom;
	private int telf,dni;
	
	public Persona(String nom, int telf, int dni) {
		this.nom = nom;
		this.telf = telf;
		this.dni = dni;
	}
	
	public Persona() {
		
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getTelf() {
		return telf;
	}

	public void setTelf(int telf) {
		this.telf = telf;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	@Override
	public String toString() {
		return "Persona [nom=" + nom + ", telf=" + telf + ", dni=" + dni + "]";
	}
}
