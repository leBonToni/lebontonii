package iam.tcaballero.museu;

public class Escultura extends Obra{
	private String material;
	private double altura;
	
	public Escultura(String titol,int numInv,String material,double altura,Artista autor) {
		super(titol,numInv,autor);
		this.material = material;
		this.altura = altura;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	@Override
	public String toString() {
		return "Escultura [titol= " + titol + " Arista= "+ autor + "NumInv= " + numInv + " material=" + material + ", altura=" + altura + "]";
	}
	
	
	
}
