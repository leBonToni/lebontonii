package iam.tcaballero.museu;

abstract public class Obra {
	String titol;
	int numInv;
	Artista autor;
	int any;
	public static int numObres = 0;
	
	public Obra() {
	}
	
	public Obra(String titol, int numInv,Artista autor){
		this.titol = titol;
		this.numInv = numInv;
		this.numObres++;
		this.autor = autor;
	}
	
	public static int getNumObres(){
		return numObres;
	}
	
	public int getNumInv() {
		return numInv;
	}
	
	public String getTitol() {
		return titol;
	}

	public void setTitol(String titol) {
		this.titol = titol;
	}

	public Artista getAutor() {
		return autor;
	}

	public void setAutor(Artista autor) {
		this.autor = autor;
	}

	public int getAny() {
		return any;
	}

	public void setAny(int any) {
		this.any = any;
	}

	public void setNumInv(int numInv) {
		this.numInv = numInv;
	}

	abstract public String toString();
	
}
