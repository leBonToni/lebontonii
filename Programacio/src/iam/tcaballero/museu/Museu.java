package iam.tcaballero.museu;

import java.util.ArrayList;

public class Museu {

	public static void main(String[] args) {
				
		ArrayList<Obra> obres = new ArrayList<Obra>();
		
		for(int i = 0; i <10; i++) {
			obres.add(new Pintura("Pintura"+i, i, i+2, i+1,new Artista("Autor" + i)));
		}
		
		for(int j = 0; j < 5; j++) {
			obres.add(new Escultura("Escultura"+j, j+10, "Marbre", j,new Artista("Autor" +j)));
		}
		
		Catalogo cataleg = new Catalogo(obres);
	
		int num = Obra.getNumObres();
		Pintura p = new Pintura("Pintura"+ num, num, num, num,new Artista("Autor"+num));
		System.out.println(cataleg.anyadeObra(p));
		System.out.println(cataleg.eliminaObra(5));
		System.out.println(cataleg.superficie());
		System.out.println(cataleg.masAlta());
		
		for(Obra o: obres) System.out.println(o);
		
	}	
}
