package iam.tcaballero.museu;

public class Artista {
	private String nom;
	private String llocNaix;
	
	public Artista(String nom){
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getLlocNaix() {
		return llocNaix;
	}

	public void setLlocNaix(String llocNaix) {
		this.llocNaix = llocNaix;
	}

	public String toString() {
		return "Artista [nom=" + nom + ", llocNaix=" + llocNaix + "]";
	}
	
	
	
}
