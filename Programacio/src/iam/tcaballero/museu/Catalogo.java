package iam.tcaballero.museu;

import java.util.ArrayList;

public class Catalogo {
	ArrayList<Obra> obres;
	
	public Catalogo (ArrayList<Obra> obres){
		this.obres = obres;
	}
	
	public String anyadeObra(Obra obra) {
		if(obres.add(obra)) return "S'ha pogut afegir";
		return "No s'ha pogut";
	}
	
	public String eliminaObra(int numInv){
		for(int i = 0; i < obres.size(); i++){
			if(obres.get(i).getNumInv() == numInv)
				if(obres.remove(obres.get(i))){
					Obra.numObres--;
					return "S'ha eliminat";
				}
		}
		return "No s'ha eliminat";		
	}
	
	public String buscarObra(int numInv) {
		String obra = null;
		for(int i = 0; i < obres.size(); i++){
			if(obres.get(i).getNumInv() == numInv);
				obra = obres.get(i).toString();
		}
		return obra;
	}
	
	public int masAlta() {
		double altura= 0;
		int numInv = 0;
		for(int i = 0; i < obres.size(); i++){
			if(obres.get(i) instanceof Escultura) {
				double n = ((Escultura) obres.get(i)).getAltura();
				if(n > altura) {
					altura = n;
					numInv = obres.get(i).getNumInv();
				}
			}
		}
		return numInv;
	}
	
	public double superficie() {
		double suma = 0;
		for(int i = 0; i < obres.size(); i++){
			if(obres.get(i) instanceof Pintura) {
				double n = ((Pintura) obres.get(i)).getAltura();
				double n2 = ((Pintura) obres.get(i)).getAmplada();
				double n3 = n * n2;
				suma +=  n3;
			}
		}
		return suma;
	}
}
