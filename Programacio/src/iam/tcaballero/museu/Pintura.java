package iam.tcaballero.museu;

public class Pintura extends Obra {
	private double altura;
	private double amplada;
	private String soport;
	
	public Pintura(String titol, int numInv, double altura,double amplada,Artista autor) {
		super(titol,numInv,autor);
		this.altura = altura;
		this.amplada = amplada;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getAmplada() {
		return amplada;
	}

	public void setAmplada(double amplada) {
		this.amplada = amplada;
	}

	public String getSoport() {
		return soport;
	}

	public void setSoport(String soport) {
		this.soport = soport;
	}

	@Override
	public String toString() {
		return "Pintura [titol= " + titol + " Artista= " + autor + " NumInv= " + numInv + " altura=" + altura + ", amplada=" + amplada + ", soport=" + soport + "]";
	}	
		
	
}
