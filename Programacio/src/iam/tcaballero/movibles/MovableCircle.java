package iam.tcaballero.movibles;

public class MovableCircle implements Movable{
	int radius;
	MovablePoint center;
	
	
	public MovableCircle (int x, int xSpeed, int y, int ySpeed,int radius) {
		this.radius = radius;
		this.center = new MovablePoint(x, xSpeed, y, ySpeed);
	}
	
	public void moveUp() {
		this.center.moveUp();
	}
	
	public void moveDown() {
		this.center.moveDown();
	}
	
	public void moveRight(){
		this.center.moveRight();
	}
	
	public void moveLeft() {
		this.center.moveLeft();
	}

}
