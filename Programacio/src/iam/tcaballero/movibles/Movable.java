package iam.tcaballero.movibles;

public interface  Movable {
	
	public default void moveUp(){
	}
	
	public default void moveDown() {
	}
	
	public default void moveRight(){
	}
	
	public default void moveLeft(){
	}
	
}
