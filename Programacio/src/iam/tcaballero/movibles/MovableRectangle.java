package iam.tcaballero.movibles;

public class MovableRectangle implements Movable {
	MovablePoint topLeft;
	MovablePoint bottomRight;
	
	public MovableRectangle (int x1,int x2, int xSpeed, int y1, int y2, int ySpeed) {
		this.topLeft = new MovablePoint(x1, xSpeed, y1, ySpeed);
		this.bottomRight = new MovablePoint(x2, xSpeed, y2, ySpeed);
		
	}
	
	public void moveUp() {
		this.topLeft.moveUp();
		this.bottomRight.moveUp();
	}
	
	public void moveDown() {
		this.topLeft.moveDown();
		this.bottomRight.moveDown();
	}
	
	public void moveRight(){
		this.topLeft.moveRight();
		this.bottomRight.moveRight();
	}
	
	public void moveLeft() {
		this.topLeft.moveLeft();
		this.bottomRight.moveRight();
	}
	
}
