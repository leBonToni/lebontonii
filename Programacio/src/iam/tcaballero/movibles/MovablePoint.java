package iam.tcaballero.movibles;

public class MovablePoint implements Movable {
	
	int x,xSpeed,y,ySpeed;
	
	public MovablePoint(int x, int xSpeed, int y, int ySpeed) {
		this.x = x;
		this.xSpeed = xSpeed;
		this.y = y;
		this.ySpeed = ySpeed;
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getxSpeed() {
		return xSpeed;
	}

	public void setxSpeed(int xSpeed) {
		this.xSpeed = xSpeed;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getySpeed() {
		return ySpeed;
	}

	public void setySpeed(int ySpeed) {
		this.ySpeed = ySpeed;
	}

	public String toString() {
		return "MovablePoint [x=" + x + ", xSpeed=" + xSpeed + ", y=" + y + ", ySpeed=" + ySpeed + "]";
	}

	public void moveUp(){
		this.y = y + ySpeed;
	}
	
	public void moveDown() {
		this.y = y - ySpeed;
	}
	
	public void moveRight() {
		this.x = x + xSpeed;
	}
	
	public void moveLeft(){
		this.x = x - xSpeed;
	}
}
