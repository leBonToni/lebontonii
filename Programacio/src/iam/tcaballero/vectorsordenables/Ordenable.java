package iam.tcaballero.vectorsordenables;

public interface Ordenable {

	public boolean menorQue(Ordenable a);
}
