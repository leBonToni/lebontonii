package iam.tcaballero.vectorsordenables;

public class OrdenaVector {
	
	public static void directSort( Ordenable v[] ) {
		Persona aux;
		for(int i = 0;i < v.length; i++) {
			for(int j=0; j < v.length; j++) {
				if(v[i].menorQue(v[j])) {
					aux = (Persona) v[i];
					v[i] = v[j];
					v[j] = aux;
				}
			}
		}
	}

	public static void imprimirVector( Imprimible v[] ) {
		for (int i = 0; i < v.length; i++) {
			v[i].imprimir();
		}
	}
}
