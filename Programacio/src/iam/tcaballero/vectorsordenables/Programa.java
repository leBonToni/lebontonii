package iam.tcaballero.vectorsordenables;

public class Programa {

	public static void main(String[] args) {

		Persona institut[] = new Persona[6];
		institut[0] = new Persona("Pere", 50);
		institut[1] = new Alumne("Ana", 30, 6);
		institut[2] = new Professor("Alex", 40, 1000);
		institut[3] = new Persona("Carles", 55);
		institut[4] = new Alumne("Pep", 25, 8);
		institut[5] = new Professor("Enric", 30, 950);

		OrdenaVector.directSort( institut );
		OrdenaVector.imprimirVector( institut );
					
		}
	}

