package iam.tcaballero.json;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import com.google.gson.Gson;

public class TestClase {

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
				
		Estudiant e = new Estudiant("Toni", "Caballero", "41015191g", "DAM", 21, 0, 10);
		Estudiant e2 = new Estudiant("Ferran", "Burriel", "00000000", "Cuina", 21, 1, 11);
		
		ArrayList<Estudiant> gEst = new ArrayList<>();
		gEst.add(e);
		gEst.add(e2);
		
		Professor pF = new Professor("Carles", "Calzina", "11111111", 29, 3);
		
		Gson gSon = new Gson();
		
		String gSonOut = gSon.toJson(e);
		
		Path p = Paths.get("Estudiant.json");
		Files.write(p, gSonOut.getBytes());
		
		String gSonIn = new String(Files.readAllBytes(p));
		
		Estudiant eGsonIn = gSon.fromJson(gSonIn,Estudiant.class);
		System.out.println(eGsonIn.toString());
		
		
		Grup g = new Grup(gEst, pF);
		
		gSonOut = gSon.toJson(g);
		p = Paths.get("Grup.json");
		Files.write(p, gSonOut.getBytes());
		
		gSonIn = new String(Files.readAllBytes(p));
		Grup gGsonIn = gSon.fromJson(gSonIn,Grup.class);
		
		for (Estudiant est : gGsonIn.getEstudiants()) {
			System.out.println(est.toString());
		}
		
	}
}
