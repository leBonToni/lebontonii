package iam.tcaballero.json;

import java.io.Serializable;

public class Professor implements Serializable{
	private String nom, cognom, dni;
	private int edat, codiProfessor;
	
	public Professor (String nom, String cognom, String dni, int edat, int codiProfessor) {
		this.nom = nom;
		this.cognom = cognom;
		this.dni = dni;
		this.edat = edat;
		this.codiProfessor = codiProfessor;
	}

	public String toString() {
		return "Professor [nom=" + nom + ", cognom=" + cognom + ", DNI=" + dni
				+ ", edat=" + edat + ", codiProfessor=" + codiProfessor + "]";
	}
	
}
