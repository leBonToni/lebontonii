package iam.tcaballero.json;

import java.io.Serializable;
import java.util.ArrayList;

//hacer un bucle metiendo datos de cada alumno
public class Grup implements Serializable{
	ArrayList<Estudiant> estudiants;
	Professor tutor;

	public Grup(ArrayList<Estudiant> estudiants, Professor tutor) {
		this.estudiants = estudiants;
		this.tutor = tutor;
	}

	public ArrayList<Estudiant> getEstudiants() {
		return estudiants;
	}

	public int numAprovats() {
		int numAprovats = 0;
		for (int i = 0; i < estudiants.size(); i++) {
			if (estudiants.get(i).getNota() >= 5)
				numAprovats++;
		}

		return numAprovats;
	}
}
