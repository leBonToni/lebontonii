package iam.tcaballero.taylor;

public class Taylor {

	public static final double PI = Math.PI;
	
	public static double getSinus(int x) {
		double n = PI * x/180;
		return (n - (Math.pow(n, 3)/3) + (Math.pow(n, 5)/120));
	}
	
	public static double getCosinus(int x) {
		double n = PI * x/180;
		return 1 - Math.pow(n, 2)/2 + Math.pow(n, 4)/24; 
	}
	
	public static double getExp(int n) {
		return (1 + n + Math.pow(n, 2)/2 + Math.pow(n, 3)/6 + Math.pow(n, 4)/24 + Math.pow(n, 5)/120); 
	}
	
	public static String autor() {
		return "Toni Caballero";
	}
}
