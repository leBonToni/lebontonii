package mordor;

import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Partida {

	public static void main(String[] args) {

		int numJugadors = Integer.parseInt(JOptionPane.showInputDialog("Número de Jugadors"));
		ArrayList<Jugador> jugPartida = new ArrayList<>();
		
		for (int i = 0; i < numJugadors ; i++ ) {
			String s = JOptionPane.showInputDialog("Raça");
			String n = JOptionPane.showInputDialog("Nom");

			if (s.equals("Elf")) {
				Elf e = new Elf(n);
				jugPartida.add(e);
			} else if (s.equals("Nan")) {
				Nan m = new Nan(n);
				jugPartida.add(m);
			} else {
				System.out.println("Raça incorrecte,torna-la a introduir-la");
				i--;
			}
		}

		Taulell t = new Taulell(jugPartida);
		t.setNumCasellesTaulell(45);
		t.setHistorialPartida("");
		
		System.out.println("Jugadors Partida:");
		for (Jugador jugador : jugPartida) {
			System.out.println(jugador.toString());
			System.out.println("Imatge: " +  jugador.dibuixarJugador());
		}
		
		boolean partidaAcabada = false;
		
		do {
			partidaAcabada = t.novaRonda();
		} while (!partidaAcabada);
		
		System.out.println(t.getHistorialPartida());
		System.out.println("Partida Acabada");
			
	}

}
