package mordor;

public class Nan extends Jugador {

	public Nan(String nombre) {
		super(nombre);
	}

	@Override
	public int tirarDau() {
		int tiradaDau = (int) ((Math.random() * 6) + 1);
		
		return tiradaDau + 4;
	}

	public String toString() {
		return "Nan " +  super.toString();
	}
	
}
