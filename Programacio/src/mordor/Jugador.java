package mordor;

public abstract class Jugador implements Dibuixable{
	
	private String nomJugador;
	private int posicioJugador;
	public static int record;
	
	
	public Jugador(String nom) {
		this.nomJugador = nom;
		this.posicioJugador = 0;
	}

	public String mortJugador() {
		this.posicioJugador = 0;
		return "Jugador " + this.getNomJugador() + " torna cap a la sortida";
	}
	
	public String moureJugador(int numCaselles) {
		this.posicioJugador += numCaselles;
		String s = "Jugador " + this.getNomJugador() + " s'ha desplaçat " + numCaselles + " i està a la posició " + this.getPosicioJugador();
	
		if (this.getPosicioJugador() > Jugador.getRecord()) {
			Jugador.setRecord(this.getPosicioJugador());
			s = s + ",PRIMER!!";
		}
		return s;
	}
	
	public String dibuixarJugador() {
		return "img/imatge.png, " + this.getPosicioJugador() + ", " + this.getNomJugador();
	}
	
	public String getNomJugador() {
		return nomJugador;
	}


	public void setNomJugador(String nomJugador) {
		this.nomJugador = nomJugador;
	}


	public int getPosicioJugador() {
		return posicioJugador;
	}


	public void setPosicioJugador(int posicioJugador) {
		this.posicioJugador = posicioJugador;
	}

	
	public static int getRecord() {
		return record;
	}


	public static void setRecord(int record) {
		Jugador.record = record;
	}

	
	public String toString() {
		String s = this.getNomJugador() + " està a la posició: " + this.getPosicioJugador();
		if (this.getPosicioJugador() == Jugador.getRecord()) {
			s = s + ", PRIMER!!";
			return s;
		}
		return s;
	}
	
	public abstract int tirarDau();

}
