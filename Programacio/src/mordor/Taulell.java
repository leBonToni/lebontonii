package mordor;

import java.util.ArrayList;

public class Taulell {
	private ArrayList<Jugador> jugadorsPartida;
	private int ronda, numCasellesTaulell;
	private String historialPartida;

	public Taulell(ArrayList<Jugador> jugadorsPartida) {
		this.jugadorsPartida = jugadorsPartida;
	}
	
	public boolean novaRonda() {
		for (Jugador j : this.getJugadors()) {
			this.historialPartida = this.getHistorialPartida() + j.moureJugador(j.tirarDau()) + "\n";
			if(j.getPosicioJugador() >= this.getNumCasellesTaulell()) {
				return true;
			 }
		}
		return false;
	}
	
	public ArrayList<Jugador> getJugadors() {
		return jugadorsPartida;
	}

	public void setJugadors(ArrayList<Jugador> jugadores) {
		this.jugadorsPartida = jugadores;
	}

	public int getRonda() {
		return ronda;
	}

	public void setRonda(int ronda) {
		this.ronda = ronda;
	}

	public int getNumCasellesTaulell() {
		return numCasellesTaulell;
	}

	public void setNumCasellesTaulell(int numCasellesTaulell) {
		this.numCasellesTaulell = numCasellesTaulell;
	}

	public String getHistorialPartida() {
		return historialPartida;
	}

	public void setHistorialPartida(String historialPartida) {
		this.historialPartida = historialPartida;
	}

}
