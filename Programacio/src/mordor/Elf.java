package mordor;

public class Elf extends Jugador {

	public Elf(String nomJugador) {
		super(nomJugador);
	}

	@Override
	public int tirarDau() {
		int primeraTirada = (int) ((Math.random() *  6) + 1);
		int segonaTirada = (int) ((Math.random() * 6) + 1);
		
		return (primeraTirada + segonaTirada) + 1;
	}

	public String toString() {
		return "Elf " +  super.toString();
	}

}
